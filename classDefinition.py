from base64 import b64decode
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import json, Crypto, pymysql, time
from passlib.hash import pbkdf2_sha256
import flask_login as fl
from datetime import datetime
from hashlib import md5
class LoginHelper(object):
    RESPONSE_CODE = {
        100: 'empty username or password',
        101: 'no account',
        102: 'wrong password',
        103: 'username is already in use',
        201: 'successful log in ',
        202: 'successful register'
    }
    def __init__(self):
        self.key = RSA.generate(2048)
        self._publicKeyString = self.key.publickey().export_key('PEM').decode("utf-8").replace("\n","",999)
        self.cipher = PKCS1_OAEP.new(self.key, hashAlgo=Crypto.Hash.SHA256)
        self.setupDataBaseConnection()
        self.userDict = {}
        self.getAllUsers()

    def getAllUsers(self):
        self.setupDataBaseConnection()
        sql = 'select id, user_name from user;'
        self.cursor.execute(sql)
        data = self.cursor.fetchall()
        self.db.close()
        for one in data:
            self.userDict[str(one[0])] = one[1]

    def decodeLoginInfor(self, data):
        # covert bytes array to string with utf8 format and then load to a dict as JSON
        loginInfor = json.loads(data.decode('utf8'))
        # username is stored as it is
        username = loginInfor['username']
        # password is bytes array encoded as 64base, decode before decryption
        encrypPassword = b64decode(loginInfor['password'])
        # get the real password
        password = self.cipher.decrypt(encrypPassword).decode('utf8')
        return [username, password]

    def getPubKey(self):
        return self._publicKeyString

    def setupDataBaseConnection(self):
        self.db = pymysql.connect(host='localhost',
                                  user='root',
                                  db='blog',
                                  charset='utf8mb4',
                                  autocommit=True
                                 )
        self.cursor = self.db.cursor()
    def login(self, username, password):
        # if username or password is empty
        if len(str(username)) == 0 or len(str(password)) == 0:
            # error code 100: empty username or password
            return 100, 0, None
        # pull infor from db
        print('Execute sql')
        self.setupDataBaseConnection()
        rowCount = self.cursor.execute('select user_password, id from user where user_account=%s',(username))
        self.db.close()
        # 0 infor is collected, return false
        if rowCount == 0:
        # error code 101: no account
            return 101, 0, None
        passwordHash, userid = self.cursor.fetchone()

        if(pbkdf2_sha256.verify(password, passwordHash)):
            # correct username and password
            return 201, userid, username
        else:
            # error code: wrong password
            return 102, 0, None

    def register(self,username, password):
        if len(str(username)) == 0 or len(str(password)) == 0:
            # error code 100: empty username or password
            return 100, 0, None
        self.setupDataBaseConnection()
        rowCount = self.cursor.execute('select user_password from user where user_account=%s', (username))
        self.db.close()
        # username already used
        if rowCount > 0:
            # error code duplicate username
            return 103, 0, None
        passwordHash = pbkdf2_sha256.hash(password)
        timeStr = str(int(time.time()))
        sql = "insert into user (user_name, user_password, user_joindate, user_account) VALUES ('{0}', '{1}', {2}, '{3}');".\
            format(username, passwordHash, timeStr, username)
        print(sql)
        self.setupDataBaseConnection()
        self.cursor.execute(sql)


        sql = 'select MAX(id) from user'
        self.cursor.execute(sql)
        data = self.cursor.fetchone()
        userid = data[0]
        self.db.close()
        # self.cursor.execute('select last_insert_id();')
        # userId = self.cursor.fetchone()[0]
        return 202, userid, username

    def processLoginRegister(self, data):
        # covert bytes array to string with utf8 format and then load to a dict as JSON
        loginInfor = json.loads(data.decode('utf8'))
        username, password = self.decodeLoginInfor(data)
        print('username: ', username, 'password: ', password)
        if (loginInfor['action'] == 'login'):
            print('Process login')
            return self.login(username, password)
        else:
            print('Process register')
            return self.register(username, password)

    def getUser(self, userId):
        if str(userId) in self.userDict:
            return userId, self.userDict[str(userId)]
        else:
            return None


class User(fl.UserMixin):
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.email = name

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()

        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
        digest, size)


class DBHelper(object):
    def __init__(self):
        pass


    def excuteSQL(self, sql):
        self.setupDBConnect()
        affectRow = self.cursor.execute(sql)
        data = self.cursor.fetchall()
        self.db.close()
        return affectRow, data


    def setupDBConnect(self):
        self.db = pymysql.connect(host='localhost',
                                  user='root',
                                  db='blog',
                                  charset='utf8mb4',
                                  autocommit=True
                                  )
        self.cursor = self.db.cursor()

    def editFollowInfor(self, user_id, following, update):
        '''
        :param data:
            user_id: The user_id who makes this action
            following: the user follorwed by user_id
            update: True for add and false for delete (un following)
        :return: True for success action and False otherwise
        '''
        try:
            timeStamp = time.time()
            if update:
                # add one follow infor
                sql = 'insert into follow (user_id, following_id, date) values ({0}, {1}, {2});'.format(
                    user_id, following, timeStamp
                )
                self.excuteSQL(sql)
            else:
                sql = "delete from follow where user_id={0} and following_id={1};".format(
                    user_id, following
                )
                self.excuteSQL(sql)
            return True
        except:
            return False

    def getFollowingByUserId(self, user_id):
        '''
        :param user_id

        :return: list of tuples [(date, following_id)] or []
        '''
        try:
            sql = 'select date, following_id from follow where user_id=' + user_id + ';'
            _, data = self.excuteSQL(sql)
            print(sql)
            return data
        except:
            return []

    def getUserNameById(self, user_id):
        '''
        :param user_id

        :return: list of tuples [(date, following_id)] or []
        '''
        try:
            sql = 'select user_name from user where id=' + str(user_id)+ ';'
            _, data = self.excuteSQL(sql)
            print(sql)
            return data[0][0]
        except:
            return 'Guest'


    def getFollowedByUserId(self, user_id):
        '''
        :param user_id

        :return: list of tuples [(date, follower_id)] or []
        '''
        try:
            sql = 'select date, user_id from follow where following_id=' + str(user_id) + ';'
            _, data = self.excuteSQL(sql)
            print(sql)
            return data
        except:
            return []

    def getFollowedCheck(self, user_id, following_id):
        '''
        :param user_id

        :return: True if user_id following following_id
        '''
        try:
            sql = 'select date from follow where following_id={0} and user_id={1}'.format(following_id, user_id)
            check, _ = self.excuteSQL(sql)
            print('Cheng go')
            return check>0
        except:
            return False


    def getNewPostId(self, user_id):
        try:
            sql = 'select max(post_id) from post where user_id='+user_id;
            _,data = self.excuteSQL(sql)
            print(data)
            if data[0][0]==None:
                # no post before
                return 1
            return data[0][0]+1
        except:
            return 0


    def getUserPostsByUserID(self, user_id):
        '''

        :param user_id:
        :return: list of tuple of ([post_id, date, like, brief]) or []
        '''
        try:
            sql = 'select post_id, date, likes, brief, content from post where user_id='+str(user_id)+';'
            _, data = self.excuteSQL(sql)
            return data
        except:
            return []

    def getAllPostsInfor(self):
        '''

        :param user_id:
        :return: list of tuple of ([post_id, date, like, brief]) or []
        '''
        try:
            sql = 'select user_id, brief from post;'
            _, data = self.excuteSQL(sql)
            return data
        except:
            return []

    def getPostByUserIDAndPostID(self, user_id, post_id):
        '''

        :param user_id and post_id
        :return: list of tuple of ([date, like, content]) or []
        '''
        try:
            sql = 'select date, likes, content, brief from post where user_id={0} and post_id={1}'.format(user_id, post_id)
            print(sql)
            _, data = self.excuteSQL(sql)
            return data
        except:
            return []

    def setPostByUserIDAndPostID(self, user_id, post_id, content, brief):
        '''

        :param user_id and post_id
        :return: 1 for successful and 0 otherwise
        '''
        try:
            sql = 'select date from post where user_id={0} and post_id={1}'.format(user_id, post_id)
            row, data = self.excuteSQL(sql)
            date = time.time()
            if row==0:
                # new post
                    post_id = self.getNewPostId(user_id)
                    sql = "insert into post(post_id, user_id, content, likes, brief, date) values ({0},{1},\"{2}\",{3},\"{4}\",{5})".format(
                        post_id, user_id, content, 0, brief, date
                    )
                    _,__ = self.excuteSQL(sql)
            else:
                # exist post
                sql = "update post set content=\"{0}\", brief=\"{1}\" where user_id={2} and post_id={3};".format(
                    content, brief, user_id, post_id
                )
                _, __ = self.excuteSQL(sql)
            return 1
        except:
            return 0

    def getTotalLikesByUserID(self, user_id):
        '''

        :param user_id:
        :return: int total like or -1 if failed
        '''
        try:
            sql = 'select sum(like) from post where user_id={0} ;'.format(user_id)
            _, data = self.excuteSQL(sql)
            return data[0]
        except:
            return -1

    def getCommnentsByUserID(self, user_id):
        '''

        :param user_id:
        :param post_id:
        :return: list of comments [date, content, commentername] or []
        '''
        try:
            sql = 'select date, content, commenter_id, anonymous from comment where user_id={0}'.format(user_id)
            _, data = self.excuteSQL(sql)
            comments = []
            for oneInfor in data:
                comment = []
                comment.append(datetime.utcfromtimestamp(int(oneInfor[0])).strftime('%Y-%m-%d'))
                comment.append(oneInfor[1])
                commenterName = self.getUserNameById(oneInfor[2])
                if (oneInfor[3]):
                    commenterName='Guest'
                comment.append(commenterName)
                comments.append(comment)
            return comments
        except:
            return []

    def updateLikeByUserIDAndPostID(self, user_id, post_id):
        '''

        :param user_id:
        :param post_id:
        :return: 1 for successful and 0 otherwise
        '''
        try:
            sql = 'update post set like=like+1 where user_id={0} and post_id={1}'.format(user_id, post_id)
            _, __ = self.excuteSQL(sql)
            return 1
        except:
            return 0

    def updateCommentByUserID(self, user_id, comment, anon, commenter_id):
        """
        :param user_id:
        :param post_id:
        :param comment:
        :return: 1 for successful and o other wise

        """
        try:
            timeStamp = int(time.time())
            sql = "insert into comment(date, content, user_id, anonymous, commenter_id) values ({0},'{1}',{2},{3},{4})".format(
            timeStamp, comment, user_id, anon, commenter_id
            )
            self.excuteSQL(sql)
            return 1
        except:
            return 0

class AnonymousUser(fl.AnonymousUserMixin):
    @property
    def name(self):
        return 'Guest'

    def avatar(self, size):
        digest = md5(self.name.lower().encode('utf-8')).hexdigest()

        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
        digest, size)
