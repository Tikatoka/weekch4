#!/Users/lir/ubuntu/shared/PythonProject/WeekCh4/venv/python
from classDefinition import LoginHelper, User, DBHelper, AnonymousUser
import json, flask_login, time
from flask import render_template, Flask, request, redirect, url_for
from flask_login import current_user, login_user, logout_user
from datetime import datetime


# generate a helper object when the server start
loginHelper = LoginHelper()
# store the public key as string for login
pubkey = loginHelper.getPubKey()
# create a login_manager for flask_login
login_manager = flask_login.LoginManager()
login_manager.anonymous_user = AnonymousUser
dbHelper = DBHelper()

# prepare the server
app = Flask(__name__)
app.secret_key = "super secret key"
login_manager.init_app(app)
app.debug = True


@login_manager.user_loader
def load_user(user_id):
    print('Try to get: ', user_id)
    if user_id == '0':
        return None
    id, username = loginHelper.getUser(user_id)
    if id != None:
        return User(id, username)
    else:
        return None



@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        # get data from the post request
        data = request.get_data()
        code, id, name = loginHelper.processLoginRegister(data)
        print(loginHelper.RESPONSE_CODE[code])
        # log in successfully
        if code == 201:
            user = User(id, name)
            loginHelper.userDict[str(id)] = user.name
            login_user(user)
            return str(id)
        if code == 202:
            user = User(id, name)
            loginHelper.userDict[str(id)] = user.name
            login_user(user)
            return str(id)
        else:
            return '/'
    return render_template('index.html', pubkey=pubkey)





@app.route('/<int:userid>/', methods=['GET','POST'])
def blog(userid):
    if request.method == 'GET':
        username = dbHelper.getUserNameById(userid)
        posts = dbHelper.getUserPostsByUserID(userid)
        # date, content, commentername
        followedNum = len(dbHelper.getFollowedByUserId(userid))
        if dbHelper.getFollowedCheck(current_user.get_id(), userid):
            followButton = 'unFollow'
        else:
            followButton = 'Follow'
            # post_id, date, likes, brief, content
        posts = list(posts)
        for i in range(len(posts)):
            post = posts[i]
            posts[i] = list(post)
            posts[i][1] = datetime.utcfromtimestamp(int(post[1])).strftime('%Y-%m-%d %H:%M:%S')
        comments = dbHelper.getCommnentsByUserID(userid)
        return render_template('index4.html', name=username, followNum=followedNum,
                               followCheck=followButton, posts=posts, comments=comments, user=current_user,
                               logoutButton=current_user.is_authenticated)
    else:
        return 'STOP'


@app.route('/edit/<int:userid>/', methods=['GET','POST'])
def addpost(userid):
    followedNum = len(dbHelper.getFollowedByUserId(userid))
    if dbHelper.getFollowedCheck(current_user.get_id(), userid):
        followButton = 'unFollow'
    else:
        followButton = 'Follow'
    username = dbHelper.getUserNameById(userid)
    comments = dbHelper.getCommnentsByUserID(userid)
    return render_template('editor.html', name=username, followNum=followedNum,
                    followCheck=followButton, comments=comments)



@app.route('/edit/<int:userid>/<blogid>', methods=['GET','POST'])
def edit(userid,blogid):
    if current_user.is_authenticated:
        followedNum = len(dbHelper.getFollowedByUserId(userid))
        data = dbHelper.getPostByUserIDAndPostID(userid, blogid)
        if dbHelper.getFollowedCheck(current_user.get_id(), userid):
            followButton = 'unFollow'
        else:
            followButton = 'Follow'
        if (len(data)==0):
            content = ''
            title = 'Title'
            date=''
        else:
            content = data[0][2]
            title = data[0][3]
            date = datetime.utcfromtimestamp(int(data[0][0])).strftime('%Y-%m-%d %H:%M:%S')
        username = dbHelper.getUserNameById(userid)
        comments = dbHelper.getCommnentsByUserID(userid)
        return render_template('editor.html', name=username, followNum=followedNum,
                    followCheck=followButton, content=content.strip(), title=title, date=date, comments=comments,
                    user=current_user, logoutButton=current_user.is_authenticated)
    else:
        # no right to edit
        return 110


@app.route('/follow', methods=['POST'])
def likeprocess():
    if current_user.is_authenticated:
        data = json.loads(request.get_data().decode('utf8'))
        user_id = current_user.get_id()
        following_id = int(data['url'].replace('/','',2))
        update = not data['update']
        dbHelper.editFollowInfor(user_id, following_id, update)
        return '/'+ str(following_id)
    else:
        return '110'


@app.route('/newpost', methods=['POST'])
def newpost():
    if current_user.is_authenticated:
        #data = json.loads(request.get_data().decode('utf8'))
        user_id = current_user.get_id()
        post_id = dbHelper.getNewPostId(user_id)
        return 'edit/' + str(user_id) + '/' + str(post_id)

    else:
        return '/'

@app.route('/save', methods=['POST'])
def savepost():
    if current_user.is_authenticated:
        data = json.loads(request.get_data().decode('utf8'))
        user_id = current_user.get_id()
        post_id = data['post_id']
        content = data['content'].replace("\"","\'",999999999)
        brief = data['brief']
        code = dbHelper.setPostByUserIDAndPostID(user_id, post_id, content, brief)
        return str(code)
    else:
        return '/'

@app.route('/myblog', methods=['GET'])
def myblog():
    if current_user.is_authenticated:
        user_id = current_user.get_id()
        return redirect('/'+str(user_id))
    else:
        return redirect(url_for('home'))


@app.route('/comment', methods=['POST'])
def comment():
    data = json.loads(request.get_data().decode('utf8'))
    if len(data['comment']) == 0:
        return '/' + str(data['user_id'])
    if (not current_user.is_authenticated) or int(data['annon']):
        annon = 1
    else:
        annon = 0
    user_id = data['user_id']
    commentor_id = current_user.get_id()
    if commentor_id == None:
        commentor_id = 0
    comment = data['comment']
    dbHelper.updateCommentByUserID(user_id, comment, annon, commentor_id)
    return '/' + str(data['user_id'])


@app.route('/editPost', methods=['POST'])
def editPost():
    if current_user.is_authenticated:
        user_id = current_user.get_id()
        data = json.loads(request.get_data().decode('utf8'))
        if int(user_id) != data['user_id']:
            return str('110')
        else:
            return "/edit/{0}/{1}".format(user_id, data['post_id'])
    else:
        # no right to edit
        return str('110')

@app.route('/logout', methods=['POST'])
def logout():
    if current_user.is_authenticated:
        logout_user()
        return '/'
    else:
        return '/'

@app.route('/explore', methods=['GET'])
def explore():
    posts = dbHelper.getAllPostsInfor()
    infors = []
    for post in posts:
        infor = []
        userid = post[0]
        infor.append(userid)
        infor.append(dbHelper.getUserNameById(userid))
        infor.append(post[1])
        infors.append(infor)
    return render_template('explore.html', infors=infors)


if __name__ == '__main__':
     app.run(debug=True)
